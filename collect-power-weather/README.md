# Collecting Power and Weather Measurements on the POWDER Platform #

The goal of this tutorial is to step through the process of collecting
both received signal strength and weather data within the POWDER Platform
over an undetermined and extended period of time. The received signal
strength values are gathered between two CBRS rooftop nodes. The weather
data is collected from the Open Weather Map API. This tutorial is currently
set up to transmit from a single CBRS rooftop node and receive from four
CBRS rooftop nodes.

## How it Works ##
The big picture idea of this tutorial is to repeatedly collect environmental
weather conditions and received signal strength measurements between a transmitter
and any number of receivers. The entire tutorial consists of a single, main python
script that runs the majority of the necessary operations directly on your
computer with two helper python scripts running on your computer as well.

The main script, [powerweather_tutorial.py](https://github.com/allisontodd/powder-summer20/blob/master/tutorials/collect-power-weather/powerweather_tutorial.py)
can be broken down into three simple parts. First, everything is set up, but most
importantly the weather API is instantiated for weather collection.

Next, six functions are created to gather temperature, rainfall, wind speed,
humidity, connection noise floor, and connection power. The first four weather
functions use the API to collect the current weather conditions appropriately. The
noise floor function calls the helper [gather_noisefloor.py](https://github.com/allisontodd/powder-summer20/blob/master/tutorials/collect-power-weather/gather_noisefloor.py)
function which I will go into more detail later. The data
from this function is then saved appropriately. Next is the power function which
calls the helper [gather_power.py](https://github.com/allisontodd/powder-summer20/blob/master/tutorials/collect-power-weather/gather_power.py) function. The data from this function is then also saved appropriately.

The last section of the script focuses on calling these functions. A writetofile()
function is used to call each of the above functions and then finally save their
outputs into a .csv file. The writetofile() function is called within a delta
timing loop so that the data collection occurs approximately every two minutes.

Now focusing on the helper scripts, [gather_noisefloor.py](https://github.com/allisontodd/powder-summer20/blob/master/tutorials/collect-power-weather/gather_noisefloor.py)
and [gather_power.py](https://github.com/allisontodd/powder-summer20/blob/master/tutorials/collect-power-weather/gather_power.py) are functionally very similar.
Because transmission is not involved, gather_noisefloor.py is much simpler as it
sets up SSH connections to each of the rooftop nodes using the python fabric
library and then calls a series of UHD commands on each of these connections. uhd_rx_cfile gathers iq reception data, iq_to_power.py converts this to power
measurements, and then this data is saved directly to your computer in the
form of a .csv file.

gather_power.py does has all this same functionality, but
also simultaneously transmits. In order for this to work, python's
multiprocessing capabilities need to be utilized. The fabric library is once
again used to create SSH connections, but this time including the transmitter
connection. A transmitter and receiver function are then created. The
receiver function is exactly the same as gather_noisefloor.py. The
transmitter function calls uhd_siggen_90sec which transmits a signal specified
through parameters for 90 seconds. In the main portion of this script, two
processes are set up, each corresponding to one of the above functions. The
transmit process begins running, then the main sleeps for 30 seconds allowing
the transmitter to reach its full value before reception. The receive process
then starts and finishes. After 90 seconds the transmit process ends and we
return to powerweather_tutorial.py.      

## Now Do It ##

### Before You Begin ###
**Create a reservation** on the POWDER Platform for at least two CBRS
rooftop nodes. The nodes that you choose will later designated as transmitter
or receiver. Following the tutorial exactly, you will need five total nodes.
Choose from these:
  * cbrssdr1-bes
  * cbrssdr1-browning
  * cbrssdr1-dentistry
  * cbrssdr1-fm
  * cbrssdr1-honors
  * cbrssdr1-meb
  * cbrssdr1-smt
  * cbrssdr1-ustar

The **reservation should also include** the same number of either d430 or d740
compute nodes depending on resource availability. It is also good practice to reserve
the 3550 to 3560 frequency range.

The weather data will be collected through the
[Open Weather Map API](https://openweathermap.org), so you will need to **create an
account** and receive an API key to be used for data collection.

The collection of the power and weather data will utilize both the
[powerweather_tutorial.py](https://github.com/allisontodd/powder-summer20/blob/master/tutorials/collect-power-weather/powerweather_tutorial.py),
[gather_noisefloor.py](https://github.com/allisontodd/powder-summer20/blob/master/tutorials/collect-power-weather/gather_noisefloor.py)
and [gather_power.py](https://github.com/allisontodd/powder-summer20/blob/master/tutorials/collect-power-weather/gather_power.py)
Python scripts. Make sure that all three of these are downloaded into
the directory you will use for data collection.

### Instantiate the Experiment ###
Using the CBRS rooftops nodes reserved above, instantiate an experiment on the
POWDER platform following these step:
  1. select **Experiments**
  2. choose **Start Experiment**
  3. select **Change Profile**
  4. find **cbrs_rx_tx**
  5. click **Select Profile** then **Next**
  6. choose the **Compute node type** you reserved (d740 or d430)
  7. adjust **frequency range** to match your reservation
  8. add the appropriate number of **X310 CBRS Radios** (5 if following tutorial)
  9. specify the **names** of the CBRS Radios you reserved
  10. click **Next**
  11. optionally **name** your experiment here then click **Next**
  12. select your reservation which sets the begin and end times
  13. click **Finish**

Now, your experiment will begin provisioning and booting up the resources you
will be using. This will take a few moments. You are ready to go once your
screen shows **"Your experiment is ready!"**

### Make the Changes ###
You now need to go into the Python code you downloaded earlier and alter certain
portions so that it will work for you and the resources you have been allocated.
This requires you know 7 pieces of information:
 1. your Open Weather Map API Key
 2. the destination file name you would like to create (.csv)
 3. the ssh password for your device
 4. your POWDER username for ssh
 5. SSH url for transmit node
 6. SSH url for reception nodes
 7. location of ssh key_filename on your device

Go into powerweather_tutorial.py, gather_noisefloor.py and gather_power.py and
make all the appropriate changes as specified below: (the line numbers correspond
to the original tutorial)

 **powerweather_tutorial.py**
  * **line 60** : add Open Weather Map API Key
  * **line 267** : add destination file name you wish to create
  * **line 21 to 57** : walks through how to add or remove receivers to match the
  number you desire

 **gather_noisefloor.py**
  * **line 36** : add ssh password + ssh url for receive node 1 + ssh username
  * **line 43** : add ssh password + ssh url for receive node 2 + ssh username
  * **line 50** : add ssh password + ssh url for receive node 3 + ssh username
  * **line 57** : add ssh password + ssh url for receive node 4 + ssh username      
  * **line 64** : add ssh url for receive node 1 + ssh username
  * **line 65** : add location of ssh key_filename
  * **line 68** : add ssh url for receive node 2 + ssh username
  * **line 69** : add location of ssh key_filename
  * **line 72** : add ssh url for receive node 3 + ssh username
  * **line 73** : add location of ssh key_filename
  * **line 76** : add ssh url for receive node 4 + ssh username
  * **line 77** : add location of ssh key_filename
  * **line 11 to 27** : walks through how to add or remove receiver to match the
  number you desire


  **gather_power.py**
   * **line 38** : add ssh password + ssh url for receive node 1 + ssh username
   * **line 45** : add ssh password + ssh url for receive node 2 + ssh username
   * **line 52** : add ssh password + ssh url for receive node 3 + ssh username
   * **line 59** : add ssh password + ssh url for receive node 4 + ssh username      
   * **line 71** : add ssh url for transmit node + ssh username
   * **line 72** : add location of ssh key_filename
   * **line 76** : add ssh url for receive node 1 + ssh username
   * **line 77** : add location of ssh key_filename
   * **line 80** : add ssh url for receive node 2 + ssh username
   * **line 81** : add location of ssh key_filename
   * **line 82** : add ssh url for receive node 3 + ssh username
   * **line 85** : add location of ssh key_filename
   * **line 88** : add ssh url for receive node 4 + ssh username
   * **line 89** : add location of ssh key_filename    

### Running It ###
Now that all the necessary changes have been made, the script can be run and the
data collection can begin!

Run powerweather_tutorial.py however you prefer to run Python scripts and watch
as both received signal strength and weather data is collected. Data is collected
approximately every two minutes and will continue being collected until you stop
running powerweather_tutorial.py.

### The Data ###
Once you are done, the data you collected will be found in the destination
file you named. The .csv file you created will have four rows of weather data
and two times the number of receivers you chose rows of received signal strength
data, but the number of columns entirely depends on the length of time the experiment was run for.
 * **First Row** : Environmental Temperature
 * **Second Row** : Rainfall
 * **Third Row** : Windspeed
 * **Fourth Row** : Percent Humidity
 * **Fifth Row** : Noise Floor of Receiver 1
 * **Sixth Row** : Received Signal Strength of Receiver 1
 * **this pattern continues for however many receivers you chose...**

 The MatLab code [plot_data.m](https://github.com/allisontodd/powder-summer20/blob/master/tutorials/collect-power-weather/plot_tutorial.m)
 is included so that you may graph the data collection.
 Be sure to change the file name on line 8 to match the destination file
 name used previously.
