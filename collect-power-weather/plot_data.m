
close all;
clear all;

%% Import Collected Data

% add data csv file name
data = readmatrix('test_data.csv');

%% Extract Weather Data

temp = data(1,:);
rain = data(2,:);
wind = data(3,:);
humid = data(4,:);

%% Extract Power Data
% You should know how many receivers were used in collection for this part.
% For this tutorial, I am using data that utalized 4 receivers 

noiseFloor1 = data(5,:);
power1 = data(6,:);
rss1 = noiseFloor1 - power1;

noiseFloor2 = data(7,:);
power2 = data(8,:);
rss2 = noiseFloor2 - power2;

noiseFloor3 = data(9,:);
power3 = data(10,:);
rss3 = noiseFloor3 - power3;

noiseFloor4 = data(11,:);
power4 = data(12,:);
rss4 = noiseFloor4 - power4;

%% Plot Power

figure;
plot(rss1);
hold on
plot(rss2);
plot(rss3);
plot(rss4);

title('Noisy Power Measurements for 4 Connections');
xlabel('Time')
ylabel('Power')
legend('Connection1','Connection2','Connection3','Connection4');

figure;
plot(movmean(rss1,20));
hold on
plot(movmean(rss2,20));
plot(movmean(rss3,20));
plot(movmean(rss4,20));

title('Power Measurements for 4 Connections');
xlabel('Time')
ylabel('Power')
legend('Connection1','Connection2','Connection3','Connection4');

%% Plot Power and Weather

graph_rss = rss3;

figure;
title('Environmental Temperature and Power');
xlabel('Time');
yyaxis left;
plot(movmean(graph_rss,20));
ylabel('RSS (db)');
hold on;
yyaxis right;
plot(temp);
ylabel('Temperature (C)');

figure;
title('Rainfall and RSS');
xlabel('Time (min)');
yyaxis left;
plot(movmean(graph_rss,20));
ylabel('RSS (db)');
hold on;
yyaxis right;
plot(rain);
ylabel('Rainfall (cm)');


figure;
title('Windspeed and RSS');
xlabel('Time (min)');
yyaxis left;
plot(movmean(graph_rss,20));
ylabel('RSS (db)');
hold on;
yyaxis right;
plot(wind);
ylabel('Windspeed (m/s)');


figure;
title('Humidity and RSS');
xlabel('Time (min)');
yyaxis left;
hold on;
plot(movmean(graph_rss,20));
ylabel('RSS (db)');
hold on;
yyaxis right;
plot(humid);
ylabel('Humidity (%)');

