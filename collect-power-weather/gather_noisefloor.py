from fabric import Connection, Config
import time
import subprocess
from multiprocessing import Process

# Currently this tutorial is set up to gather the power of *4*
# reception radios and *1* transmision radio. This is what I am currently
# using for my research, but it is very simple to add or subtract reception
# radios as you please. This tutorial works best with a single tranmission radio

# Gather_noisefloor.py requires you to either copy or delete connections based
# on how many reception radios you want.
#   In the receive() method:
#       Copy or delete the code block for each connection you want
#           "connection1.run('uhd_rx_cfile -f 3555e6 --lo-offset=1.2M -N 10000 test_bin_power1')
#           connection1.run('iq_to_power.py -p ~/test_bin_power1 -w 5000 -n bin_db_power1')
#           subprocess.call('pscp -pw *SSH PASSWORD* -scp *USERNAME*@pc*PC NUMBER*.emulab.net:~/bin_db_power1.csv gather_noisefloor1.csv')"
#       Make sure to change all the appropriate numbers: connection#, test_bin_power#,
#       bin_db_power#, and gather_noisefloor#.csv
#   Setting up the connection:
#       Copy or delete the code block for each connection you want
#           "receive_ssh_1 = Connection(host='pc*PC NUMBER*.emulab.net', user='*USERNAME*',
#            connect_kwargs={'key_filename': '*ADD KEY LOCATION*'})"
#       Make sure to change all the appropriate numbers: receive_ssh_#,
#   Calling the receive() method:
#       Edit the parameter of the receive() function to match the connections
#       just made, appropriately add, deleting, and changing numbers.

def receive(connection1, connection2, connection3, connection4):
    print('in recieve')
    # runs uhd_rx_cfile on connection 1
    connection1.run('uhd_rx_cfile -f 3555e6 --lo-offset=1.2M -N 10000 test_bin_power1')
    # turns the data from above into power measurements
    connection1.run('iq_to_power.py -p ~/test_bin_power1 -w 5000 -n bin_db_power1')
    # writes the data from the radio to your device
    subprocess.call('pscp -pw *SSH PASSWORD* -scp *USERNAME*@pc*PC NUMBER*.emulab.net:~/bin_db_power1.csv gather_noisefloor1.csv')

    # runs uhd_rx_cfile on connection 2
    connection2.run('uhd_rx_cfile -f 3555e6 --lo-offset=1.2M -N 10000 test_bin_power1')
    # turns the data from above into power measurements
    connection2.run('iq_to_power.py -p ~/test_bin_power1 -w 5000 -n bin_db_power1')
    # writes the data from the radio to your device
    subprocess.call('pscp -pw *SSH PASSWORD* -scp *USERNAME*@pc*PC NUMBER*.emulab.net:~/bin_db_power1.csv gather_noisefloor2.csv')

    # runs uhd_rx_cfile on connection 3
    connection3.run('uhd_rx_cfile -f 3555e6 --lo-offset=1.2M -N 10000 test_bin_power1')
    # turns the data from above into power measurements
    connection3.run('iq_to_power.py -p ~/test_bin_power1 -w 5000 -n bin_db_power1')
    # writes the data from the radio to your device
    subprocess.call('pscp -pw *SSH PASSWORD* -scp *USERNAME*@pc*PC NUMBER*.emulab.net:~/bin_db_power1.csv gather_noisefloor3.csv')

    # runs uhd_rx_cfile on connection 4
    connection4.run('uhd_rx_cfile -f 3555e6 --lo-offset=1.2M -N 10000 test_bin_power1')
    # turns the data from above into power measurements
    connection4.run('iq_to_power.py -p ~/test_bin_power1 -w 5000 -n bin_db_power1')
    # writes the data from the radio to your device
    subprocess.call('pscp -pw *SSH PASSWORD* -scp *USERNAME*@pc*PC NUMBER*.emulab.net:~/bin_db_power1.csv gather_noisefloor4.csv')

    print('end recieve')
    return

# sets up reception connection 1
# Add correct SSH location for receive node + location of SSH key_filename
receive_ssh_1 = Connection(host='pc*PC NUMBER*.emulab.net', user='*USERNAME*',
                connect_kwargs={'key_filename': '*ADD KEY LOCATION*'})

# sets up reception connection 2
receive_ssh_2 = Connection(host='pc*PC NUMBER*.emulab.net', user='*USERNAME*',
                connect_kwargs={'key_filename': '*ADD KEY LOCATION*'})

# sets up reception connection 3
receive_ssh_3 = Connection(host='pc*PC NUMBER*.emulab.net', user='*USERNAME*',
                connect_kwargs={'key_filename': '*ADD KEY LOCATION*'})

# sets up reception connection 4
receive_ssh_4 = Connection(host='pc*PC NUMBER*.emulab.net', user='*USERNAME*',
                connect_kwargs={'key_filename': '*ADD KEY LOCATION*'})

# runs the receive function on the connection just set up
receive(receive_ssh_1,receive_ssh_2, receive_ssh_3, receive_ssh_4)
