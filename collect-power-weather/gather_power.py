from fabric import Connection, Config
import time
import subprocess
from multiprocessing import Process

# Currently this tutorial is set up to gather the power of *4*
# reception radios and *1* transmision radio. This is what I am currently
# using for my research, but it is very simple to add or subtract reception
# radios as you please. This tutorial works best with a single tranmission radio

# Gather_power.py requires you to either copy or delete connections based
# on how many reception radios you want.
#   In the receive() method:
#       Copy or delete the code block for each connection you want
#           "connection1.run('uhd_rx_cfile -f 3555e6 --lo-offset=1.2M -N 10000 test_bin_power#')
#           connection1.run('iq_to_power.py -p ~/test_bin_power# -w 5000 -n bin_db_power#')
#           subprocess.call('pscp -pw *SSH PASSWORD* -scp *USERNAME*@pc*PC NUMBER*.emulab.net:~/bin_db_power#.csv gather_power#.csv')"
#       Make sure to change all the appropriate numbers: connection#, test_bin_power#,
#       bin_db_power#, and gather_power#.csv
#   Setting up the connection:
#       Copy or delete the code block for each connection you want
#           "receive_ssh_# = Connection(host='pc*PC NUMBER*.emulab.net', user='*USERNAME*',
#            connect_kwargs={'key_filename': '*ADD KEY LOCATION*'})"
#       Make sure to change all the appropriate numbers: receive_ssh_#,
#   Calling the receive() method:
#       Edit the parameter of the receive() function to match the connections
#       just made, appropriately add, deleting, and changing numbers.


# reception function:
def receive(connection1, connection2, connection3, connection4):
    print('in recieve')
    # runs uhd_rx_cfile on connection 1
    connection1.run('uhd_rx_cfile -f 3555e6 --lo-offset=1.2M -N 10000 test_bin_power1')
    # turns the data from above into power measurements
    connection1.run('iq_to_power.py -p ~/test_bin_power1 -w 5000 -n bin_db_power1')
    # writes the data from the radio to your device
    subprocess.call('pscp -pw *SSH PASSWORD* -scp *USERNAME*@pc*PC NUMBER*.emulab.net:~/bin_db_power1.csv gather_power1.csv')

    # runs uhd_rx_cfile on connection 1
    connection2.run('uhd_rx_cfile -f 3555e6 --lo-offset=1.2M -N 10000 test_bin_power1')
    # turns the data from above into power measurements
    connection2.run('iq_to_power.py -p ~/test_bin_power1 -w 5000 -n bin_db_power1')
    # writes the data from the radio to your device
    subprocess.call('pscp -pw *SSH PASSWORD* -scp *USERNAME*@pc*PC NUMBER*.emulab.net:~/bin_db_power1.csv gather_power2.csv')

    # runs uhd_rx_cfile on connection 1
    connection3.run('uhd_rx_cfile -f 3555e6 --lo-offset=1.2M -N 10000 test_bin_power1')
    # turns the data from above into power measurements
    connection3.run('iq_to_power.py -p ~/test_bin_power1 -w 5000 -n bin_db_power1')
    # writes the data from the radio to your device
    subprocess.call('pscp -pw *SSH PASSWORD* -scp *USERNAME*@pc*PC NUMBER*.emulab.net:~/bin_db_power1.csv gather_power3.csv')

    # runs uhd_rx_cfile on connection 1
    connection4.run('uhd_rx_cfile -f 3555e6 --lo-offset=1.2M -N 10000 test_bin_power1')
    # turns the data from above into power measurements
    connection4.run('iq_to_power.py -p ~/test_bin_power1 -w 5000 -n bin_db_power1')
    # writes the data from the radio to your device
    subprocess.call('pscp -pw *SSH PASSWORD* -scp *USERNAME*@pc*PC NUMBER*.emulab.net:~/bin_db_power1.csv gather_power4.csv')
    print('end recieve')
    return

def transmit(connection):
    print('start transmit:')
    # runs uhd_siggen_90sec, transmits specified signal for 90 seconds
    connection.run('uhd_siggen_90sec --const --freq 3555e6 --amplitude 1 --gain 31.5 -v')
    print('end transmit!')

# sets up transmit connection
# Add correct SSH location for transmit node + location of SSH key_filename
transmit_ssh = Connection(host='pc*PC NUMBER*.emulab.net', user='*USERNAME*',
                connect_kwargs={'key_filename': '*ADD KEY LOCATION*'})

# sets up reception connection 1
# Add correct SSH location for receive node + location of SSH key_filename
receive_ssh_1 = Connection(host='pc*PC NUMBER*.emulab.net', user='*USERNAME*',
                connect_kwargs={'key_filename': '*ADD KEY LOCATION*'})

# sets up reception connection 2
receive_ssh_2 = Connection(host='pc*PC NUMBER*.emulab.net', user='*USERNAME*',
                connect_kwargs={'key_filename': '*ADD KEY LOCATION*'})

# sets up reception connection 3
receive_ssh_3 = Connection(host='pc*PC NUMBER*.emulab.net', user='*USERNAME*',
                connect_kwargs={'key_filename': '*ADD KEY LOCATION*'})

# sets up reception connection 4
receive_ssh_4 = Connection(host='pc*PC NUMBER*.emulab.net', user='*USERNAME*',
                connect_kwargs={'key_filename': '*ADD KEY LOCATION*'})

# In order to transmit and receive simultaneously, multiprocessing must be used
if __name__ == '__main__':
    # sets up the transmit process
    p1 = Process(target = transmit, args = (transmit_ssh,))
    # sets up the reception process
    p2 = Process(target = receive, args = (receive_ssh_1,receive_ssh_2, receive_ssh_3, receive_ssh_4,))

    # starts transmision
    p1.start()
    # sleeping for 30 seconds allows the tranmission process plenty of time to see a signal
    time.sleep(30)
    print('start recieve process')
    # starts reception process
    p2.start()
    # reception stops when all power data was been collected from each connection
    # tranmision stops after 90 seconds
