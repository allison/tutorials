import requests
import json
import csv
import pandas
import numpy
import schedule
import time
from fabric import Connection, Config
from invoke import Responder
from csv import reader, writer
import getpass
import subprocess
import matplotlib.pyplot as plt
from multiprocessing import Process

# Currently this tutorial is set up to gather the power of *4*
# reception radios and *1* transmision radio. This is what I am currently
# using for my research, but it is very simple to add or subtract reception
# radios as you please. This tutorial works best with a single tranmission radio

# powerweather_tutorial.py requires you to either copy or delete certain portions
#  of the code based on how many reception radios you want.
#   Instantiating arrays:
#       Copy or delete noiseFloor and power arrays depending on the number of connections
#           noiseFloor# = []
#           power# = []
#       Make sure to change all the appropriate numbers: noiseFloor#, power#,
#   get_noisefloor() function:
#       Copy or delete the code block for each connection you want
#              "with open('gather_noisefloor#.csv', newline='') as csvFile:
#                  noiseFloorReader = csv.reader(csvFile, quoting=csv.QUOTE_NONNUMERIC)
#                  for row in noiseFloorReader:
#                       current_noisefloor = row
#               noiseFloor#.append(current_noisefloor[0]) "
#       Fix return statement
#           return noiseFloor#, noiseFloor#, noiseFloor#...
#       Make sure to change all the appropriate numbers: gather_noisefloor#, noiseFloor#
#   get_power() function
#       Copy or delete the code block for each connection you want
#           "with open('gather_power#.csv', newline='') as csvFile:
#                powerreader = csv.reader(csvFile, quoting=csv.QUOTE_NONNUMERIC)
#                for row in powerreader:
#                    current_power = row
#            power#.append(current_power[0])"
#       Fix return statement
#           return power#, power#, power# ...
#       Make sure to change all the appropriate numbers: gather_power#, power#
#   writetofile() funtion:
#       Calling get_noisefloor() and get_power()
#           Fix return statements to match function's return statements
#               "noise#, noise#, noise# ... = get_noisefloor()
#               power#, power#, power# ... = get_power()"
#       Fix csv writer
#           Copy or delete this code block for each connection
#           "weather_writer.writerow(noise#) # write noise 1 data
#           weather_writer.writerow(power#) # write power 1 data"
#       Make sure to change all the appropriate numbers: noise#, power#

# Add your Open Weather Map api key here:
api_key = "*ADD OPEN WEATHER MAP API KEY*"
base_url = "http://api.openweathermap.org/data/2.5/weather?"

# POWDER Location is Salt Lake City, UT, this is the latitude and longitude
lat = '40.765229'
lon = '-111.846208'

#complete url that is used to access weather data
complete_url = base_url + "appid=" + api_key + "&lat=" + lat +"&lon=" + lon +"&units=metric"


#instantiate arrays used to save the different pieces of information:
temps = []  #Temperature
rain = []   #Rainfall
windspeed = []  #Windspeed
humid = []  #Percent humidity

noiseFloor1 = []    #Noise floor connection 1
noiseFloor2 = []    #Noise floor connection 2
noiseFloor3 = []    #Noise floor connection 3
noiseFloor4 = []    #Noise floor connection 4

power1 = [] #Power connection 1
power2 = [] #Power connection 2
power3 = [] #Power connection 3
power4 = [] #Power connection 4


#Temperature function: gathers temperature data
def get_temp(url):
    # get method of requests module, return response object
    response = requests.get(complete_url)
    # json method of response object convert json format data into
    # python format data
    x = response.json()
    # store the value of "main", key in variable y
    y = x["main"]
    # store the value corresponding to the "temp" key of y
    current_temperature = y["temp"]
    #appends current temperature to the cumulative temperature array
    temps.append(current_temperature)
    #returns entire temperature array
    return temps

#Rainfall function
def get_rain(url):
    # get method of requests module, return response object
    response = requests.get(complete_url)
    # json method of response object convert json format data into
    # python format data
    x = response.json()
    # store the value of "main", key in variable y
    try:
        b = x["rain"]
    except:
        #take care of case with no rain
        current_rain = 0
    else:
        current_rain = b["1h"]
    finally:
        #appends current rainfall to cumulative rainfall array
        rain.append(current_rain)

    #return rainfall array
    return rain

#Wind speed function
def get_wind_speed(url):
    # get method of requests module, return response object
    response = requests.get(complete_url)
    # json method of response object convert json format data into
    # python format data
    x = response.json()
    # store the value of "main", key in variable y
    a = x["wind"]

    current_wind_speed = a["speed"]
    #wind_degree = a["deg"]

    #appends current wind speeds to cumulative wind speeds
    windspeed.append(current_wind_speed)
    #return windspeed array
    return windspeed

def get_humid(url):
    # get method of requests module, return response object
    response = requests.get(complete_url)
    # json method of response object convert json format data into
    # python format data
    x = response.json()
    # store the value of "main", key in variable y
    y = x["main"]
    # store the value corresponding to the "temp" key of y
    current_humid = y["humidity"]
    #appends current humidity to cumulative humidity array
    humid.append(current_humid)
    #returns humidity array
    return humid

def get_noisefloor():
    print('get noisefloor')
    #calls gather_noisefloor.py and runs
    subprocess.call('python gather_noisefloor.py')
    # gather_noisefloor.py returns (in this case) 4 .csv files that hold information

    # open file 1:
    with open('gather_noisefloor1.csv', newline='') as csvFile:
        # read file 1:
        noiseFloorReader = csv.reader(csvFile, quoting=csv.QUOTE_NONNUMERIC)
        # extract information from file
        for row in noiseFloorReader:
            current_noisefloor = row
    # save important, relevant information
    noiseFloor1.append(current_noisefloor[0])

    # open file 2:
    with open('gather_noisefloor2.csv', newline='') as csvFile:
        # read file 2:
        noiseFloorReader = csv.reader(csvFile, quoting=csv.QUOTE_NONNUMERIC)
        # extract information from file
        for row in noiseFloorReader:
            current_noisefloor = row
    # save important, relevent information
    noiseFloor2.append(current_noisefloor[0])

    # open file 3:
    with open('gather_noisefloor3.csv', newline='') as csvFile:
        # read file 3:
        noiseFloorReader = csv.reader(csvFile, quoting=csv.QUOTE_NONNUMERIC)
        # extract information from file
        for row in noiseFloorReader:
            current_noisefloor = row
    # save important relevent information
    noiseFloor3.append(current_noisefloor[0])

    #open file 4:
    with open('gather_noisefloor4.csv', newline='') as csvFile:
        # read file 4:
        noiseFloorReader = csv.reader(csvFile, quoting=csv.QUOTE_NONNUMERIC)
        # extract information from file
        for row in noiseFloorReader:
            current_noisefloor = row
    # save important, relevent information
    noiseFloor4.append(current_noisefloor[0])

    # return all the important information
    return noiseFloor1, noiseFloor2, noiseFloor3, noiseFloor4


def get_power():
    print('get power')
    #calls gather_power.py and runs
    subprocess.call('python gather_power.py')
    # gather_power.py returns (in this case) 4 .csv files that hold information

    # open file 1:
    with open('gather_power1.csv', newline='') as csvFile:
        # read file 1:
        powerreader = csv.reader(csvFile, quoting=csv.QUOTE_NONNUMERIC)
        # extract information from file
        for row in powerreader:
            current_power = row
    # save important relevent information
    power1.append(current_power[0])

    # open file 2:
    with open('gather_power2.csv', newline='') as csvFile:
        # read file 2:
        powerreader = csv.reader(csvFile, quoting=csv.QUOTE_NONNUMERIC)
        # extract information from file
        for row in powerreader:
            current_power = row
    # save important relevent information
    power2.append(current_power[0])

    # open file 3:
    with open('gather_power3.csv', newline='') as csvFile:
        # read file 3:
        powerreader = csv.reader(csvFile, quoting=csv.QUOTE_NONNUMERIC)
        # extract information from file
        for row in powerreader:
            current_power = row
    # save important relevent information
    power3.append(current_power[0])

    # open file 4:
    with open('gather_power4.csv', newline='') as csvFile:
        # read file 4:
        powerreader = csv.reader(csvFile, quoting=csv.QUOTE_NONNUMERIC)
        # extract information from file
        for row in powerreader:
            current_power = row
    # save important relevent information
    power4.append(current_power[0])

    # return all the important information
    return power1, power2, power3, power4

def writetofile(url):
    temps = get_temp(complete_url)  # runs temp function
    rain = get_rain(complete_url)   # runs rain function
    wind = get_wind_speed(complete_url) # runs wind function
    humid = get_humid(complete_url) # runs humidity function
    noise1, noise2, noise3, noise4 = get_noisefloor()   # runs noisefloor function
    power1, power2, power3, power4 = get_power()    # runs power function

    # writes the results of these functions to a .csv file
    with open('*INSERT FILE NAME*.csv', 'w', newline='') as weather_file:
        weather_writer = csv.writer(weather_file, delimiter=',', quoting=csv.QUOTE_MINIMAL)
        weather_writer.writerow(temps)  # write temp data
        weather_writer.writerow(rain)   # write rain data
        weather_writer.writerow(wind)   # write wind data
        weather_writer.writerow(humid)  # write humidity data
        weather_writer.writerow(noise1) # write noise 1 data
        weather_writer.writerow(power1) # write power 1 data
        weather_writer.writerow(noise2) # write noise 2 data
        weather_writer.writerow(power2) # write power 2 data
        weather_writer.writerow(noise3) # write noise 3 data
        weather_writer.writerow(power3) # write power 3 data
        weather_writer.writerow(noise4) # write noise 4 data
        weather_writer.writerow(power4) # write power 4 data

currentTime = time.time()

# runs continuous delta time loop so that entire process occurs once every ~2 minutes
while True:
    if time.time() - currentTime >= 27:
        # gathers weather and power using url to get weather data
        writetofile(complete_url)
        currentTime = time.time()
